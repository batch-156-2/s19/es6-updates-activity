// alert('Connected')

const getCube = (number) => {
	console.log(`The cube of ${number} is ${number**3}`)
};
getCube(2)

const address = (houseNum, cityTown, country, zipcode) => {
	console.log(`I live at ${houseNum} ${cityTown}, ${country}, ${zipcode}.`)
};
address(114, 'Bacnotan', 'Philippines', 2515)

const animal = (name, type, weight, length) => {
	console.log(`${name} is a ${type}, he weighs about ${weight} kilograms, and measures ${length} centimeters.`)
};
animal('Mogy', 'grizzly bear', 290, 180)

let numbers = [1,2,3,4,5]
numbers.forEach(function(number){
	console.log(`${number}`)
});

let total = numbers.reduce((previous, current) => previous + current);
console.log(total);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const dog = new Dog('Mycroft', 1, 'Chow');
console.log(dog);